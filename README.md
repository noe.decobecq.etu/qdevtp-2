# Documentation depuis les tests

Les tests d'un projet visent 3 objectifs :

* La documentation fonctionnelle du projet
* La non régression
* Faire émerger le code

Il existe différentes façons d'utiliser les tests pour documenter un projet. 

* Utiliser des noms de méthode explicite évoquant avec précision les différentes règles métier.
* Construire un DSL de test avec le vocabulaire du métier.
* Utiliser du Gherkin pour décrire les scénarios de tests
* Générer des fichiers structurés de documentation depuis les tests

C'est cette dernière option que nous allons faire aujourd'hui.

# Une caisse de bar restaurant

L'objectif est de produire une librairie java et sa documentation pour calculer le bon taux de TVA à appliquer sur un ensemble de lignes de ticket de caisse en fonction du lieu de vente.

Notre calculateur de TVA a la signature suivante :

```java
public class VatCalculator {
    public VatCalculator(PointOfSale pointOfSale);
    public List<TicketLine> compute(CartItem... items);
}
```

Vous trouverez un ensemble de classes à compléter pour faire fonctionner le projet.

## Étape 1 : Génération du markdown

Ajouter une classe `MarkdownWriter` qui répond au besoin suivant : 
* Est instancié avec en paramètre un nom de fichier.
* Comporte une méthode `addLines(List<String>)` permettant d'accumuler des lignes de texte
* Comporte une méthode `write()` qui écrit toutes les lignes qui ont été ajouté dans le fichier `target/nom de fichier passé au constructeur.md`

Vous pouvez utiliser les méthodes suivantes :
```java
Path path = Paths.get("target", fileName);
Files.write(path, markdown.getBytes());
```

Transformer la classe de test `VatCalculatorTest` en utilisant `MarkdownWriter` pour qu'elle produise un fichier `target/french_vat.md` à chaque exécution des tests contenant pour l'instant les lignes suivantes : 

```markdown
# Calculateur de TVA Française pour la restauration

```

Transformer le test `test_meal_in_restaurant` pour qu'il ajoute les lignes suivantes dans le fichier `target/french_vat.md` **uniquement en cas de succès du test** : 

```markdown
## Vente d'un plat à manger sur place dans un restaurant en métropole
La TVA applicable est de 10%.

Par exemple :
Carbonade : 20€ TTC 
    dont TVA 10% : 1,82€
```

Faire passer le test au vert. Le fichier `target/french_vat.md` doit maintenant contenir les lignes suivantes : 

```markdown
# Calculateur de TVA
## Vente d'un plat à manger sur place dans un restaurant en métropole
La TVA applicable est de 10%.

Par exemple :
Carbonade : 20€ TTC 
    dont TVA 10% : 1,82€
```

Ajouter le cas de test d'un plat à emporter. Il va générer le petit bout de markdown suivant :
```markdown
## Vente d'un plat à emporter (le point de vente n'a pas de place assise) en métropole
La TVA applicable est de 5,5%.

Par exemple :
Grande frite : 6€ TTC 
    dont TVA 5,5% : 0,31€
```

## Étape 2 : toutes les règles Françaises

Rajouter progressivement toutes les règles de TVA applicable en France à la nourriture et aux boissons accompagnés d'une documentation en Markdown.
Chaque règle de calcul de TVA sera accompagnée d'au moins un ou deux exemples explicites.

Vous trouverez l'ensemble de ces règles sur les sites suivants :
* https://entreprendre.service-public.fr/vosdroits/F23567
* https://bofip.impots.gouv.fr/bofip/7204-PGP.html

## Étape 3 : Le cas des menus

Un menu est un lot de marchandises et en france depuis le cas Free, les lots doivent être ventilés en respectant les proratas des prix hors lot. 

Ajouter la possibilité de faire des menus et d'afficher les taux de TVA du menu. 

Ex 1:
Étant donné la commande suivante prise à la carte :

| Produit       | Prix TTC | TVA | Ratio sur la facture |
|---------------|----------|-----|----------------------|
| Poulet grillé | 12€      | 10% | 60%                  |
| Bière du mois | 8€       | 20% | 40%                  |

Avec les mêmes produits prix dans un menu sur place à 15€ boisson comprise, le plat TTC doit représenter 60% des 15€ et la boisson TTC doit représenter 40% des 15€ donc le ticket de caisse doit indiquer :

```
Menu boisson comprise : 15€ TTC 
  dont TVA 10% : 0,82€
       TVA 20% : 1,00€
```

En effet : 
* 15€ × 60% = 9€ TTC donc 8.18€ HT soit 0,82€ de TVA à 10%
* 15€ × 40% = 6€ TTC donc 6.00€ HT soit 1,00€ de TVA à 20%

Ex 2:
Étant donné la commande suivante prise à la carte :

| Produit            | Prix TTC | TVA | Ratio sur la facture |
|--------------------|----------|-----|----------------------|
| Carbonade Flamande | 17€      | 10% | 85%                  |
| Chti cola          | 3€       | 10% | 15%                  |

Avec les mêmes produits prix dans un menu sur place à 15€ boisson comprise, le plat TTC doit représenter 85% des 15€ et la boisson TTC doit représenter 15% des 15€ donc le ticket de caisse doit indiquer :

```
Menu boisson comprise : 15€ TTC 
  dont TVA 10% : 1,36€
```

En effet :
* 15€ × 85% = 14.45€ TTC donc 12.75€ HT soit 1.16€ de TVA à 10%
* 15€ × 15% = 2.25€ TTC donc 2.05€ HT soit 0.20€ de TVA à 10%

## Étape 4 : Le cas des notes de frais

Au moment de payer l'addition, certains payent exactement ce qu'ils ont pris, les autres divisent l'addition en parts égales, la TVA aussi. Ajouter l'édition de notes de frais à votre API.
