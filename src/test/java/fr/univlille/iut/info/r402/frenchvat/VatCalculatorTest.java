package fr.univlille.iut.info.r402.frenchvat;

import org.junit.jupiter.api.Test;

import java.util.List;

import static fr.univlille.iut.info.r402.frenchvat.Department.Nord;
import static fr.univlille.iut.info.r402.frenchvat.VAT.Intermediate;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class VatCalculatorTest {
    @Test
    void test_meal_in_restaurant() {
        // Given
        CartItem carbonade = new CartItem("Carbonade", 20, false, false, false, 1);
        PointOfSale brasserieJean = new PointOfSale("Brasserie Jean", true, Nord);
        // When
        VatCalculator vatCalculator = new VatCalculator(brasserieJean);
        List<TicketLine> ticketLines = vatCalculator.compute(carbonade);
        // Then
        TicketLine ticketLine = new TicketLine("Carbonade", 18.18, 1, Intermediate, 1.82);
        assertEquals(List.of(ticketLine), ticketLines);
    }


}
